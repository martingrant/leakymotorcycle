﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using RenderHelpers;
using Player;
using InputControl;
using LevelManagers;

namespace Screens
{
    class ScreenGame : ScreenAbstract
    {
        LevelManager levels;
       
        public ScreenGame() 
        {
            Console.Out.WriteLine("Game screen constructor");
        }

        override public void init(ContentManager iContent, SpriteBatch iSpriteBatch, InputControls iInputControl)
        {
            content = iContent;
            spriteBatch = iSpriteBatch;
            inputControl = iInputControl;

            levels = new LevelManager(content, iInputControl);

            Console.Out.WriteLine("hello from game screen");
        }

        override public void draw() 
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            
   
            levels.draw(ref spriteBatch);
            spriteBatch.End();
        }

        override public void update(ScreenManager manager)
        {
           
            levels.update(content);

            inputControl.checkInput();

            if (inputControl.getKey(Keys.Back)) 
                manager.setCurrentScreen(0);
          
        }
    }
}
