﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using RenderHelpers;
using Player;
using Platforms;
using InputControl;
using Objectives; 
using Powerups;
using Time;
#endregion

namespace GameLevels
{
    abstract class Level
    {
        public abstract void init(ContentManager content, InputControls iInputControl);
        public abstract void draw(ref SpriteBatch spriteBatcher, TimeSpan timestep);
        public abstract void update(TimeSpan timestep, ContentManager content);
    }

    class LevelOne : Level
    {
        TextureObject background;
        InputControls inputControl;
        TextSprite roundOverText, gameWinnerText;
        PowerUps powersup;
        Timer timer;

        private float moveSpeed = 0.3f; 
        private int powerUpCounter = 0;
        private int lastingTime = 192;
        private int roundCounter = 0;
        private bool roundActive = true;
        private bool endGame = false;

        const int maxPlayers = 4;
        int numberOfPlayers = 4; // set this one to players playing instead of the one above
        Players[] player;

        const int numberOfBases = 9;
        Objective[] playerBases = new Objective[numberOfBases];

        const int numberOfPlatforms = 10;
        Platform[] platforms = new Platform[numberOfPlatforms];

        public LevelOne()
        {
        }

        override public void init(ContentManager content, InputControls iInputControl)
        {
            inputControl = iInputControl;

            numberOfPlayers = inputControl.getNoPads();

            player = new Players[numberOfPlayers];

            background = new TextureObject(content, "environment_003.png", 0, 0, 1280, 720);
 
            // ContentManager content, String fileName, int posX, int posY, int width, int height
            string[] blue = { "blue_run_animation_001.png", "blue_run_animation_002.png", "blue_run_animation_003.png", "blue_run_animation_004.png", "blue_run_animation_005.png", "blue_run_animation_006.png", "blue_run_animation_007.png", "blue_run_animation_008.png", 
                                "blue_run_animation_009.png", "blue_run_animation_010.png", "blue_run_animation_011.png", "blue_run_animation_012.png", "blue_run_animation_013.png", "blue_run_animation_014.png", "blue_run_animation_015.png", "blue_run_animation_016.png",
                            "blue_run_animation_017.png", "blue_run_animation_018.png", "blue_run_animation_019.png", "blue_run_animation_020.png", "blue_run_animation_021.png", "blue_run_animation_022.png", "blue_run_animation_023.png", "blue_run_animation_024.png"};

            string[] green = { "green_run_animation_001.png", "green_run_animation_002.png", "green_run_animation_003.png", "green_run_animation_004.png", "green_run_animation_005.png", "green_run_animation_006.png", "green_run_animation_007.png", "green_run_animation_008.png", 
                                "green_run_animation_009.png", "green_run_animation_010.png", "green_run_animation_011.png", "green_run_animation_012.png", "green_run_animation_013.png", "green_run_animation_014.png", "green_run_animation_015.png", "green_run_animation_016.png",
                            "green_run_animation_017.png", "green_run_animation_018.png", "green_run_animation_019.png", "green_run_animation_020.png", "green_run_animation_021.png", "green_run_animation_022.png", "green_run_animation_023.png", "green_run_animation_024.png"};

            string[] pink = { "pink_run_animation_001.png", "pink_run_animation_002.png", "pink_run_animation_003.png", "pink_run_animation_004.png", "pink_run_animation_005.png", "pink_run_animation_006.png", "pink_run_animation_007.png", "pink_run_animation_008.png", 
                                "pink_run_animation_009.png", "pink_run_animation_010.png", "pink_run_animation_011.png", "pink_run_animation_012.png", "pink_run_animation_013.png", "pink_run_animation_014.png", "pink_run_animation_015.png", "pink_run_animation_016.png",
                            "pink_run_animation_017.png", "pink_run_animation_018.png", "pink_run_animation_019.png", "pink_run_animation_020.png", "pink_run_animation_021.png", "pink_run_animation_022.png", "pink_run_animation_023.png", "pink_run_animation_024.png"};

            string[] yellow = { "yellow_run_animation_001.png", "yellow_run_animation_002.png", "yellow_run_animation_003.png", "yellow_run_animation_004.png", "yellow_run_animation_005.png", "yellow_run_animation_006.png", "yellow_run_animation_007.png", "yellow_run_animation_008.png", 
                                "yellow_run_animation_009.png", "yellow_run_animation_010.png", "yellow_run_animation_011.png", "yellow_run_animation_012.png", "yellow_run_animation_013.png", "yellow_run_animation_014.png", "yellow_run_animation_015.png", "yellow_run_animation_016.png",
                            "yellow_run_animation_017.png", "yellow_run_animation_018.png", "yellow_run_animation_019.png", "yellow_run_animation_020.png", "yellow_run_animation_021.png", "yellow_run_animation_022.png", "yellow_run_animation_023.png", "yellow_run_animation_024.png"};

            string[][] playerColours = { blue, green, pink, yellow };

            for (int i = 0; i < numberOfPlayers; i++)
            {
                int offset = 200 + (i * 100);
                player[i] = new Players(content, playerColours[i], offset, 642, i);
            }

            roundOverText = new TextSprite("myFont", content, "Round Over, Press Start", new Vector2(500, 300), Color.White);
            gameWinnerText = new TextSprite("myFont", content, "Placeholder Text", new Vector2(250, 300), Color.White);

            powersup = new PowerUps();
            powersup.RandomLocationSpawner(content);
          
            // (int iValue, int iCaptureTime, ContentManager content, string fileName, int posX, int posY, int width, int height)

            string[] towers = { "environment_003_tower_neutral.png", "environment_003_tower_blue.png", "environment_003_tower_green.png", "environment_003_tower_pink.png", "environment_003_tower_yellow.png" };
            playerBases[0] = new Objective(3, 5000, content, towers, "circle", 60, 500, 50, 50);    // bottom left left
            playerBases[1] = new Objective(5, 7000, content, towers, "circle", 240, 330, 60, 60);   // mid left
            playerBases[2] = new Objective(5, 7000, content, towers, "circle", 420, 170, 60, 60);   // top left
            playerBases[3] = new Objective(3, 5000, content, towers, "circle", 420, 500, 50, 50);   // bottom left
            playerBases[4] = new Objective(7, 9000, content, towers, "circle", 600, 320, 70, 70);   // mid mid
            playerBases[5] = new Objective(5, 7000, content, towers, "circle", 780, 170, 60, 60);   // top right
            playerBases[6] = new Objective(3, 5000, content, towers, "circle", 780, 500, 50, 50);   // bottom right
            playerBases[7] = new Objective(5, 7000, content, towers, "circle", 960, 330, 60, 60);   // mid right
            playerBases[8] = new Objective(3, 5000, content, towers, "circle", 1140, 500, 50, 50);  // bottom right right


            // (ContentManager content, int xpos, int ypos, int width , int height)
            platforms[0] = new Platform(content, 0, 710, 1280, 10);     // ground
            platforms[1] = new Platform(content, 10, 550, 180, 10);     // bottom left left
            platforms[2] = new Platform(content, 190, 390, 180, 10);    // mid left
            platforms[3] = new Platform(content, 370, 230, 180, 10);    // top left
            platforms[4] = new Platform(content, 370, 550, 180, 10);    // bottom left
            platforms[5] = new Platform(content, 550, 390, 180, 10);    // mid mid
            platforms[6] = new Platform(content, 730, 230, 180, 10);    // top right
            platforms[7] = new Platform(content, 730, 550, 180, 10);    // bottom right
            platforms[8] = new Platform(content, 910, 390, 180, 10);    // mid right
            platforms[9] = new Platform(content, 1090, 550, 180, 10);   // bottom right right

            timer = new Timer();
            timer.init(60);
            timer.initDisplay(content, "MyFont", "Time Left ", new Vector2(570, 0)); 
        }

        override public void draw(ref SpriteBatch spriteBatcher, TimeSpan timestep)
        {
            background.drawMe(ref spriteBatcher);
            

            for (int i = 0; i < numberOfPlatforms; i++)
                platforms[i].DrawPlatform(ref spriteBatcher);

            for (int i = 0; i < numberOfBases; i++)
                playerBases[i].drawTower(ref spriteBatcher);

            powersup.DrawPowerUp(ref spriteBatcher);

            for (int i = 0; i < numberOfPlayers; i++)
            {
                player[i].DrawPlayer(ref spriteBatcher, timestep);
            }
           
            timer.drawMe(ref spriteBatcher);
            
            if (endGame == true)
            {
                gameWinnerText.drawMe(ref spriteBatcher, new Vector2(2, 1));
            }

            if (roundActive == false && endGame != true)
            {
                roundOverText.drawMe(ref spriteBatcher, new Vector2(2, 1));
            }

        }

        private void roundOver(ContentManager content)
        {
            // set round active to false I.E so we have a break in between rounds
            roundActive = false;
                
            // updating all the animations frames(normal update only does it for 
            // the current frame and when its paused...it doesn't animate them so
            // you get a frame on it's own just chilling somewhere)
            for (int i = 0; i < numberOfPlayers; i++)
            {
                int offset = 200 + (i * 100);
                player[i].getTextureObject().updateAllFramesPosition(offset, 662);
            }
            // Not sure it this should be one or two for loops so i split them up just incase
            for (int i = 0; i < numberOfPlayers; i++)
            {
                int offset = 200 + (i * 100);
                player[i].updatePosition(offset, 662);
            }

            powersup.RandomLocationSpawner(content);


            for (int i = 0; i < numberOfPlayers; i++)
            {
                for (int j = 0; j < numberOfBases; j++)
                {
                    if (playerBases[j].getInControl() == player[i].getPlayerID() + 1)
                    {
                        player[i].updateScore(playerBases[j].getValue());

                        // set back to netural
                        playerBases[j].reset();
                    }
                }
            }

            // increment the current round
            roundCounter++;
        }

        public bool gameOver()
        {
            // if 3 rounds have been played it's game over, time to decide a winner!
            if (roundCounter == 3)
            {
                endGame = true;
              
                Players tempPlayer;

                tempPlayer = player[0];

                // get the games winner, doesn't handle draws yet should be easy enough
                for(int i = 1; i < numberOfPlayers; i++)
                {
                    if (player[i].getScore() >= tempPlayer.getScore())
                        tempPlayer = player[i];
                }

                gameWinnerText.updateTextDisplay("Congratulations Player " + (tempPlayer.getPlayerID() + 1) + " You Win! With " + tempPlayer.getScore() + " Points");
                
                return true;
            }
           

            return false;
        }

        /*
        public int[] getScores()
        {
            int[] score = new int[numberOfPlayers];
            for (int i = 0; i < numberOfPlayers; i++)
                score[i] = player[i].getScore();
            return score;
        }
        */

        public void ResetMovementSpeed(Players player)
        {
            player.setMoveSpeed(0.3f);
        }

        //1 is slow and 2 is frozen
        public void moveSpeedModifiers(int currentPowerUp, Players player)
        {
            if (currentPowerUp == 0) { ResetMovementSpeed(player); player.notFreeze = true; }
            if (currentPowerUp == 1) { player.setMoveSpeed(moveSpeed / 2.0f); }
            if (currentPowerUp == 2) { player.setMoveSpeed(0); player.notFreeze = false; }
        }

        override public void update(TimeSpan timestep, ContentManager content)
        {
            // TODO: Add your update logic here

            // if the round is over check if the games over
            // round over resets things to there position if the timers up
            // game over will find the winner, end the game and display it
            if (roundActive == false || endGame == true)
            {
                // check if it's game over I.E round 3 has just finished, and then we calculate the overall winner
                if (!gameOver())
                {
                    // press start to move onto the next round
                    for (int i = 0; i < numberOfPlayers; i++)
                    {
                        if ( inputControl.buttonDown(i, Buttons.Start) )
                        {
                            roundActive = true;
                            timer.setTimer(60);
                            break;
                        }
                    }
                }
       
            }
            else
            {
           
                int timePastSinceLastFunction = 0;
                timePastSinceLastFunction += Convert.ToInt32(timestep.Milliseconds);

                if (timePastSinceLastFunction > 1000 / 48)
                {
                    for (int i = 0; i < numberOfPlayers; i++)
                    {
                        player[i].update(timestep, inputControl, i, platforms, numberOfPlatforms, ref player, numberOfPlayers);
                    }

                    powersup.update();

                    for (int i = 0; i < numberOfPlayers; i++)
                    {
                        if (player[i].Character.getBbox().Intersects(powersup.powerUp.bbox))
                        {
                            powersup.pickUpPowerUp(player[i]);
                            player[i].hasPowerUp = true;
                            powersup.changeXandY(1500, 1500);
                        }

                        if (player[i].usedPowerUp)
                        {
                            powerUpCounter++;
                            if (powerUpCounter >= lastingTime)
                            {
                                player[i].powerUp = 0;
                                for (int j = 0; j < numberOfPlayers; j++)
                                {
                                    moveSpeedModifiers(player[i].powerUp, player[j]);

                                }
                                player[i].usedPowerUp = false;
                                powerUpCounter = 0;
                                powersup.RandomLocationSpawner(content);
                            }
                        }
                    }
                    for (int i = 0; i < numberOfPlayers; i++)
                    {
                        if (inputControl.buttonDown(i, Buttons.B) && player[i].hasPowerUp)
                        {
                            for (int j = 0; j < numberOfPlayers; j++)
                            {
                                if( !player[j].Equals(player[i]) )
                                    moveSpeedModifiers(player[i].powerUp, player[j]);
                            }
                            player[i].hasPowerUp = false;
                            player[i].usedPowerUp = true;
                            powersup.pickUpPowerUp(player[i]);
                        }
                    }

                    timePastSinceLastFunction = 0;
                }

                for (int i = 0; i < numberOfBases; i++)
                {
                    for (int i2 = 0; i2 < numberOfPlayers; i2++)
                    {
                        playerBases[i].currentStatus(player[i2], timestep);
                    }
                }

                for (int i = 0; i < numberOfBases; i++)
                {
                    playerBases[i].getTowerTextureObject().updateBounding();
                }

                timer.update();

                // calls round over when the timer is up and set to true, basically calculates
                // score and resets things
                if (timer.getTimer() == true)
                    roundOver(content);
            }

        }
    }


  
}

