﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using InputControl;

namespace Screens
{
    abstract class ScreenAbstract
    {
        protected ContentManager content;
        protected SpriteBatch spriteBatch;
        protected InputControls inputControl;

        public abstract void init(ContentManager iContent, SpriteBatch iSpriteBatch, InputControls iInputControl);
        public abstract void draw();
        public abstract void update(ScreenManager manager);
    }
}
 