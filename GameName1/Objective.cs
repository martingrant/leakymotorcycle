﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Player;
using RenderHelpers;

#endregion

namespace Objectives
{

    class Objective
    {
        SoundEffect towerSound;
        SoundEffectInstance towerLoop;

        private int transparencyCounter;
        private int captureTimePercentage; // basically to stop a division every update
        private int value;  // How many point this objective is worth
        private int captureTime;    // How long it takes to capture this objective
        private int captureCounter; // The current time left to capture the tower
        private int inControl;   // Identifies which player is in control of the objective
                                 // 0 = nobody, 1 = player 1... etc.

        public bool blocked;

        private Color indicatorColor;

        private float transparency;

        private TextureObject captureIndicator;
        private TextureObject tower;    // Contains draw, position and bounding box also contains functions to move object and bounding boxes
        private TextureObject[] towers = new TextureObject[5];    // Contains draw, position and bounding box also contains functions to move object and bounding boxes
        

        public Objective(int iValue, int iCaptureTime, ContentManager content, string[] fileName, string indicatorFileName, int posX, int posY, int width, int height)
        {
            value = iValue;
            captureTime = iCaptureTime;
            captureCounter = 0;
            blocked = false;
            inControl = 0;
            captureTimePercentage = captureTime / 10;

            towerSound = content.Load<SoundEffect>("towereffect.wav");
            towerLoop = towerSound.CreateInstance();
            
            // super hacky way to do this, could just have 5 different texures and switch them with textureobject instead, but too tired to deal
            // with the syntax at the moment
            tower = new TextureObject(content, fileName[0], posX, posY, width, height);

            captureIndicator = new TextureObject(content, indicatorFileName, posX + (width / 2) - 10, posY - (height / 2), 20, 20);
 
            towers[0] = tower;
            towers[1] = new TextureObject(content, fileName[1], posX, posY, width, height);
            towers[2] = new TextureObject(content, fileName[2], posX, posY, width, height);
            towers[3] = new TextureObject(content, fileName[3], posX, posY, width, height);
            towers[4] = new TextureObject(content, fileName[4], posX, posY, width, height);
        }


        public int getValue()
        {
            return value;
        }


        public int getInControl()
        {
            return inControl;
        }


        // just resets it back to what it was before being captured
        public void reset()
        {
            inControl = 0;
            transparency = 0;
            tower = towers[0];
        }


        private void empty(Players player, TimeSpan timestep)
        {
            // Check bounding boxes for a collision
            // Increase captureCounter if the captureCounter is less than captureTime
            // Else decrease the captureCounter

            if (tower.bbox.Intersects(player.Character.getBbox()) && player.getPlayerID()+1 != inControl)
            {
                if(captureCounter < captureTime)
                {
                    captureCounter+=Convert.ToInt32(timestep.Milliseconds);
                    transparencyCounter += Convert.ToInt32(timestep.Milliseconds);

                    if (transparencyCounter > captureTimePercentage)
                    {
                        if (player.getPlayerID() == 0)
                            indicatorColor = Color.Blue;
                        else if (player.getPlayerID() == 1)
                            indicatorColor = Color.Green;
                        else if (player.getPlayerID() == 2)
                            indicatorColor = Color.Pink;
                        else if (player.getPlayerID() == 3)
                            indicatorColor = Color.Yellow;

                        transparency += 0.1f;
                        transparencyCounter = 0;
                    }
                }
            }
            else
            {
                if (captureCounter > captureTime)
                   captureCounter-= Convert.ToInt32(timestep.Milliseconds);
            }
           
        }


        public void giveControl(Players player)
        {
            int playerID = player.getPlayerID();
            switch (playerID+1)
            {
                case 1:
                    inControl = 1;
                    tower = towers[1];
                    captureCounter = 0;
                    transparency = 0.0f;
                    break;
                case 2:
                    inControl = 2;
                    tower = towers[2];
                    captureCounter = 0;
                    transparency = 0.0f;
                    break;
                case 3:
                    inControl = 3;
                    tower = towers[3];
                    captureCounter = 0;
                    transparency = 0.0f;
                    break;
                case 4:
                    inControl = 4;
                    tower = towers[4];
                    captureCounter = 0;
                    transparency = 0.0f;
                    break;
                default:
                    inControl = 0;
                    transparency = 0.0f;
                    Console.WriteLine(player.getPlayerID());
                    Console.WriteLine("Error playerID not between 1-4");
                    break;

            }
            towerLoop.Play();
        }


        public TextureObject getTowerTextureObject()
        {
            return tower;
        }


        // draw tower and indicator, indicators transparency changes the overall alpha of the object
        // to make it fade in/out
        public void drawTower(ref SpriteBatch spriteBatcher)
        {
            tower.drawMe(ref spriteBatcher);
            captureIndicator.drawMe(ref spriteBatcher, indicatorColor, transparency);
        }


        public void currentStatus(Players player,TimeSpan timestep)
        {
            // This function checks if the objective has been captured.
            // If not run empty function
            // If captureCounter is greater than or equal to captureTime
            // run giveControl functions
            if (inControl >= 0 && blocked == false)
            {
                empty(player, timestep);

                if (captureCounter >= captureTime) { giveControl(player); }
            }
        }

    }

}
