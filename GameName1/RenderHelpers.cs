﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace RenderHelpers
{
    public class TextureObject
    {
        public Texture2D text;
        public Rectangle rect;
        public BoundingBox bbox;

        public TextureObject() { }

        // for texture objects
        public TextureObject(ContentManager content, string fileName, int posX, int posY, int width, int height)
        {
            rect.Width = width;
            rect.Height = height;
            rect.X = posX;
            rect.Y = posY;

            text = content.Load<Texture2D>(fileName);
        }

        // should work not ideal though
        public void updateBounding()
        {
            bbox = new BoundingBox(new Vector3(rect.X, rect.Y, 0), new Vector3(rect.X + (rect.Width), rect.Y + (rect.Height), 0));
        }

        public void updatePosition(int posX, int posY)
        {
            rect.X = posX;
            rect.Y = posY;
        }

        public void updateTexture(Texture2D nText)
        {
            text = nText;
        }

        public void drawMe(ref SpriteBatch spriteBatcher)
        {
            spriteBatcher.Draw(text, rect, Color.White);
        }

        public void drawMe(ref SpriteBatch spriteBatcher, Color textureTint, float transparency)
        {
            spriteBatcher.Draw(text, rect, textureTint * transparency);
        }

        public void drawMe(ref SpriteBatch spriteBatcher, bool flip)
        {
            //   Vector2 test = new Vector2(0);
            if (flip == false)
                spriteBatcher.Draw(text, rect, Color.White);
            else

                spriteBatcher.Draw(text, rect, null, Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 0);
        }
    };

    // this class would definetly not be ideal for adding in more animations, would have to add a state class for the character to deal wit
    // multiple animation switches.
    public class AnimatedSprite
    {

        int currentFrame = 0;
        int frameCount = 0;
        int millisecondPerFrame = 0;
        int timeSinceLastStep = 0;
        bool flipped = false;

        TextureObject[] animationFrames;
        BoundingBox compositeBBox; // cheap and easy way to deal with the bbox not gonna be accurate for wildy changing animation

        public AnimatedSprite(ContentManager content, int numberOfFrames, string[] fileNames, int posX, int posY, int width, int height) 
        {
            animationFrames = new TextureObject[numberOfFrames];

            // 1 second in miliseconds / frames to get the number of frames we need to go through per second to display the animation
            // may not be ideal for sprite sheets with lower numbers of sprites, could easily add in a manual set though if required
            millisecondPerFrame = 1000 / numberOfFrames;
          
            frameCount = numberOfFrames;

            for (int i = 0; i < numberOfFrames; i++)
            {
                animationFrames[i] = new TextureObject(content, fileNames[i], posX, posY, width, height);
            }

            compositeBBox = new BoundingBox(new Vector3(posX, posY, 0), new Vector3(posX + (width), posY + (height), 0)); 
        }

        public void animate(ref SpriteBatch spriteBatcher, TimeSpan timeStep)
        {
            timeSinceLastStep += Convert.ToInt32(timeStep.Milliseconds);

            if (timeSinceLastStep >= millisecondPerFrame)
            {
                this.drawMe(ref spriteBatcher);
                currentFrame += 1;
                timeSinceLastStep = 0;
            }

            if (currentFrame == frameCount - 1)
                currentFrame = 0;
        }

        public BoundingBox getBbox()
        {
            return compositeBBox;                                 //animationFrames[currentFrame].bbox;    
        }

        public TextureObject getTextureObject()
        {
            return animationFrames[0];
        }

        public void setFlipped(bool nVal)
        {
            flipped = nVal;
        }

        public void updatePosition(int posX, int posY)
        {
            animationFrames[currentFrame].updatePosition(posX, posY);
        }

        // essentially should only be used to position the whole animation during pause
        public void updateAllFramesPosition(int posX, int posY)
        {
            for (int i = 0; i < frameCount - 1; i++)
            {
                animationFrames[i].updatePosition(posX, posY);
            }
        }

        public void updateBounding()
        {
            compositeBBox = new BoundingBox(new Vector3(animationFrames[0].rect.X, animationFrames[0].rect.Y, 0), new Vector3(animationFrames[0].rect.X + (animationFrames[0].rect.Width), animationFrames[0].rect.Y + (animationFrames[0].rect.Height), 0));
            //animationFrames[currentFrame].updateBounding();
        }

        private void drawMe(ref SpriteBatch spriteBatcher)
        {
            animationFrames[currentFrame].drawMe(ref spriteBatcher, flipped);
        }
    }

    public class TextSprite
    {

        SpriteFont font;
        string textToDisplay;
        Vector2 position;
        Color textColor;

        public TextSprite(string fontName, ContentManager content, string textDisplay, Vector2 pos, Color colorOfText)
        {
            font = content.Load<SpriteFont>(fontName);
            position = pos;
            textToDisplay = textDisplay;
            textColor = colorOfText;
        }

        public void drawMe(ref SpriteBatch spriteBatcher)
        {
            spriteBatcher.DrawString(font, textToDisplay, position, textColor);
        }

        public void drawMe(ref SpriteBatch spriteBatcher, Vector2 size)
        {
            spriteBatcher.DrawString(font, textToDisplay, position, textColor, 0, new Vector2(0, 0), size, SpriteEffects.None, 0);
        }

        public void updateColor(Color newColor)
        {
            textColor = newColor;
        }

        public void updatePosition(Vector2 pos)
        {
            position = pos;
        }

        public void updateTextDisplay(string newText)
        {
            textToDisplay = newText;
        }

    }
}
