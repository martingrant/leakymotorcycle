﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbstractScreen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using InputControl;
using RenderHelpers;

namespace Screens
{
    class ScreenMenu : ScreenAbstract
    {
        TextureObject background;
        TextureObject startButton;
        TextureObject creditsButton;

        public ScreenMenu() 
        {
            Console.Out.WriteLine("Menu screen constructor");
        }

        override public void init(ContentManager iContent, SpriteBatch iSpriteBatch, InputControls iInputControl)
        {
            content = iContent;
            spriteBatch = iSpriteBatch;
            inputControl = iInputControl;

            background = new TextureObject(content, "gamestartscreen.png", 0, 0, 1280, 720);
            startButton = new TextureObject(content, "startbutton.png", 300, 500, 258, 94);
            creditsButton = new TextureObject(content, "credits.png", 700, 500, 258, 94);
       }

       
        override public void draw()
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

            background.drawMe(ref spriteBatch);
            startButton.drawMe(ref spriteBatch);
            creditsButton.drawMe(ref spriteBatch);

            spriteBatch.End();
        }

        override public void update(ScreenManager manager)
        {
            if (inputControl.mouseOverButton(300, 500, 258, 94))
            {
                if (inputControl.mouseLeftClick()) 
                    manager.setCurrentScreen(1);
            }

            if (inputControl.mouseOverButton(700, 500, 258, 94))
            {
                if (inputControl.mouseLeftClick())
                    manager.setCurrentScreen(2);
            }

            if (inputControl.getKey(Keys.Space)) 
                manager.setCurrentScreen(1);
            else if (inputControl.getKey(Keys.C)) 
                manager.setCurrentScreen(2);

            if (inputControl.getKey(Keys.Escape)) 
                System.Environment.Exit(0);
        }
    }
}
