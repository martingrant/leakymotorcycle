﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using RenderHelpers;
using InputControl;
using Platforms;
using Time;
using Projectile;


namespace Player
{
    class Players
    {
        public AnimatedSprite Character;
        private TextSprite playerName;
        private TextSprite playerScore;
        private TextureObject frozen;
        private Projectiles push;

        private int playerID;
        public int powerUp;
        private int gravity = 10;
        private int jumpCounter = 0;
        private int jumpTime = 12;
        private int lastPosY;
        private int score;
        public int pushForce = 0;

        private double moveSpeed = 0.3;
        public double currentspeedX = 0;

        public bool hasPowerUp = false;
        private bool isImmune;
        private bool affectedByGravity = false;
        private bool jumped = false;
        public bool notFreeze = true;
        public bool usedPowerUp = false;
        public bool projectileFired = false;

        public Players(ContentManager content, string[] fileName, int xpos, int ypos, int ID)
        {
            Character = new AnimatedSprite(content, 24, fileName, xpos, ypos, 35, 48);
            frozen = new TextureObject(content, "ice_active", xpos, ypos, 66, 55);
            push = new Projectiles();

            lastPosY = ypos;

            score = 0;
            playerID = ID;

            Vector2 scorePos = new Vector2(50, 0);

            if(playerID == 1)
                scorePos = new Vector2(300, 0);
            else if(playerID == 2)
                scorePos = new Vector2(820, 0);
            else if (playerID == 3)
                scorePos = new Vector2(1070, 0);
            
           
            playerName = new TextSprite("MyFont", content, "Player " + (playerID + 1), new Vector2(xpos, ypos), Color.White);
            playerScore = new TextSprite("MyFont", content, "Player " + (playerID + 1) + " Score: " + score, scorePos, Color.White);
        }

        public void updatePosition(int posX, int posY)
        {
            Character.updatePosition(posX, posY);
            playerName.updatePosition(new Vector2(Character.getTextureObject().rect.X - 10, Character.getTextureObject().rect.Y - 30));
        }

        public int getScore()
        {
            return score;
        }

        public void updateScore(int s)
        {
            score += s;
            playerScore.updateTextDisplay("Player " + (playerID + 1) + " Score " + score);
        }

/*        public void playerCollider(ref Players[] player, int id, int numberOfPlayers, InputControls iInputControl)
        {
            int playerid = id;
            for (int i = 0; i < numberOfPlayers; i++)
            {

                if (i != id)
                {

                    if (this.Character.getBbox().Intersects(player[i].Character.getBbox()) && i != playerid)
                    {
                        if (this.Character.getTextureObject().rect.X < player[i].Character.getTextureObject().rect.X)
                        {
                            player[i].pushForce = 10;
                        }
                        else if (this.Character.getTextureObject().rect.X > player[i].Character.getTextureObject().rect.X)
                        {
                            player[i].pushForce = -10;
                        }
                    }

                }

            }
        }*/

        public void move(TimeSpan itimestep, float direction)
        {
            if (direction < 0)
                Character.setFlipped(true);
            else
                Character.setFlipped(false);

            int timestep;

            timestep = Convert.ToInt32(itimestep.TotalMilliseconds);
            if (pushForce != 0)
                currentspeedX = (timestep * 1.0 * moveSpeed) * pushForce;
            else
                currentspeedX = (timestep * direction * moveSpeed);
            Character.getTextureObject().rect.X += Convert.ToInt32(currentspeedX);
            if (pushForce != 0)
                pushForce = 0;

        }

        public void update(TimeSpan timestep, InputControls iInputControl, int id, Platform[] platforms, int numberOfPlatforms, ref Players[] player, int numberOfPlayers)
        {
            affectedByGravity = true;

            Character.updateBounding();
          
            push.playerCollider(ref player, id, numberOfPlayers, timestep, projectileFired);

            move(timestep, iInputControl.getAxis(id, "leftX"));

            for (int i = 0; i < numberOfPlatforms; i++)
            {
                if (this.Character.getBbox().Intersects(platforms[i].block.bbox))
                {
                    if (this.Character.getTextureObject().rect.Y + this.Character.getTextureObject().rect.Height <= platforms[i].block.rect.Y)
                    {
                        affectedByGravity = false;
                        // below offset code doesn't appear to be needed works fine without it
                        // if any problems occur with platforms uncomment and see if it fixes!
                        // int offsetY = (Character.getTextureObject().rect.Y) - lastPosY;
                        // this.Character.getTextureObject().rect.Y = (this.Character.getTextureObject().rect.Y - offsetY);
                    }
                }
            }

            Character.updateBounding();

            if (iInputControl.buttonDown(playerID, Buttons.A))
            {
                if (!affectedByGravity && !jumped && notFreeze)
                {
                    jumped = true;
                    affectedByGravity = true;
                    gravity = -gravity * 2;
                }
            }

            if (Character.getTextureObject().rect.X > 1280)
            {
                Character.getTextureObject().rect.X = 10;

                Character.updateBounding();
            }

            if (Character.getTextureObject().rect.X < 0)
            {
                Character.getTextureObject().rect.X = 1240;

                Character.updateBounding();
            }

            if (affectedByGravity)
            {
                Character.getTextureObject().rect.Y += gravity;
                Character.updateBounding();
            }

            if (jumped)
            {
                jumpCounter++;
                if (jumpCounter >= jumpTime)
                {
                    gravity = -gravity / 2;
                    jumpCounter = 0;
                    jumped = false;
                }
            }

            updatePosition(Character.getTextureObject().rect.X, Character.getTextureObject().rect.Y);
            lastPosY = Character.getTextureObject().rect.Y;
        }

        public void setMoveSpeed(float newSpeed)
        {
            moveSpeed = newSpeed;
        }

        private void draw(ref SpriteBatch spriteBatcher)
        {
            frozen.updatePosition(this.Character.getTextureObject().rect.X, this.Character.getTextureObject().rect.Y);
            frozen.drawMe(ref spriteBatcher);
        }

        public void DrawPlayer(ref SpriteBatch spriteBatcher, TimeSpan timestep)
        {
            if (notFreeze)
            {
                Character.animate(ref spriteBatcher, timestep);
            }
            else
            {
                draw(ref spriteBatcher);
            }
            playerName.drawMe(ref spriteBatcher);
            playerScore.drawMe(ref spriteBatcher);
        }

        public void PowerUps(int powerup)
        {
            powerUp = powerup;
        }

        public void immunity(bool immune)
        {
            isImmune = immune;
        }

        public bool ComparePlayers(Players otherPlayer)
        {
            int x = this.getPlayerID().CompareTo(otherPlayer.getPlayerID());
            if (x == 0)
            {
                return true;
            }
            else
                return false;
        }
        public AnimatedSprite getTextureObject()
        {
            return Character;
        }

        public int getPlayerID()
        {
            return playerID;
        }

    }
}
