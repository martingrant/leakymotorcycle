﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using RenderHelpers;

namespace Platforms
{
    class Platform
    {
        private int xPos;
        private int yPos;
        public TextureObject block;


        public Platform(ContentManager content, int xpos, int ypos, int width , int height)
        {
            xPos = xpos;
            yPos = ypos;
            block = new TextureObject(content, "Orange.bmp", xPos, yPos, width, height);
            block.updateBounding();
        }

        public void DrawPlatform(ref SpriteBatch spriteBatcher)
        {
            block.drawMe(ref spriteBatcher);
        }
    }
}
