﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using RenderHelpers;
using GameLevels;
using InputControl;
using System.Diagnostics;
using System.Threading;
#endregion

namespace LevelManagers
{
    class LevelManager
    {
        Level currentLevel;
        Level previousLevel;
        Level firstLevel;
        //Level secondLevel;
        //Level thirdLevel;
        //Level fourthLevel;

        Stopwatch time;

        TimeSpan elapsedTime;
        InputControls inputControl;

        public LevelManager(ContentManager content, InputControls iInputControl)
        {
            time = new Stopwatch();

            firstLevel = new LevelOne();
            //secondLevel = new LevelTwo();
            //thirdLevel = new LevelThree();
            

            firstLevel.init(content, iInputControl);
            //secondLevel.init(content, iInputControl);
           // thirdLevel.init(content, iInputControl);

            inputControl = iInputControl;

            currentLevel = firstLevel;
            previousLevel = currentLevel;

         
            elapsedTime = TimeSpan.FromMilliseconds(41.67); // 24 FPS.

            //time.Start();
        }

        public void update(ContentManager content)
        {
            currentLevel.update(elapsedTime, content);
            //currentLevel.update(time.Elapsed, content);
        }

        public void draw(ref SpriteBatch spriteBatcher)
        {
            currentLevel.draw(ref spriteBatcher, elapsedTime);
            //currentLevel.draw(ref spriteBatcher, time.Elapsed);
           // time.Restart();
        }

        /*
        public void switchLevel(int levelNum)
        {
            if (levelNum == 0)
            {
                previousLevel = currentLevel;
                currentLevel = firstLevel;
            }
            else if (levelNum == 1)
            {
                previousLevel = currentLevel;
                currentLevel = secondLevel;
            }
        }
        */

       
    }
}
