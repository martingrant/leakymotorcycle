﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using InputControl;
using RenderHelpers;

namespace Screens
{
    class ScreenCredits : ScreenAbstract
    {
        TextureObject background;
        TextureObject backButton;

        public ScreenCredits()
        {
            Console.Out.WriteLine("Credits screen constructor");
        }

        override public void init(ContentManager iContent, SpriteBatch iSpriteBatch, InputControls iInputControl)
        {
            content = iContent;
            spriteBatch = iSpriteBatch;
            inputControl = iInputControl;

            background = new TextureObject(content, "creditsscreen.png", 0, 0, 1280, 720);
            backButton = new TextureObject(content, "backbutton.png", 900, 150, 258, 94);
        }

        override public void draw()
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

            background.drawMe(ref spriteBatch);
            backButton.drawMe(ref spriteBatch);

            spriteBatch.End();
        }

        override public void update(ScreenManager manager) 
        {
            if (inputControl.mouseOverButton(900, 150, 258, 94))
            {
                if (inputControl.mouseLeftClick()) 
                    manager.setCurrentScreen(0);
            }
        }
    }
}